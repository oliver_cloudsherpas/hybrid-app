//Sample code for Hybrid REST Explorer

var selected_contact = new Array(100);
var pangalan = "";
var selected_account = new Array(100);
var akawnt = "";
function regLinkClickHandlers() {
    var $j = jQuery.noConflict();
    var logToConsole = cordova.require("salesforce/util/logger").logToConsole;
    $j('#link_fetch_device_contacts').click(function() {
                                           logToConsole("link_fetch_device_contacts clicked");
                                           var contactOptionsType = cordova.require("cordova/plugin/ContactFindOptions");
                                           var options = new contactOptionsType();
                                           options.filter = ""; // empty search string returns all contacts
                                           options.multiple = true;
                                           var fields = ["name"];
                                           var contactsObj = cordova.require("cordova/plugin/contacts");
                                           contactsObj.find(fields, onSuccessDevice, onErrorDevice, options);
                                           });
    
    $j('#link_fetch_sfdc_contacts').click(function() {
                                         logToConsole("link_fetch_sfdc_contacts clicked");
                                         forcetkClient.query("SELECT Name FROM Contact", onSuccessSfdcContacts, onErrorSfdc); 
                                         });
    
    $j('#link_fetch_sfdc_accounts').click(function() {
                                         logToConsole("link_fetch_sfdc_accounts clicked");
                                         forcetkClient.query("SELECT Name FROM Account", onSuccessSfdcAccounts, onErrorSfdc); 
                                         });
    
    $j('#link_reset').click(function() {
                           logToConsole("link_reset clicked");
                           $j("#div_device_contact_list").html("")
                           $j("#div_sfdc_contact_list").html("")
                           $j("#div_sfdc_account_list").html("")
                           $j("#console").html("")
                           });
                           
    $j('#link_logout').click(function() {
             logToConsole("link_logout clicked");
             var sfOAuthPlugin = cordova.require("salesforce/plugin/oauth");
             sfOAuthPlugin.logout();
             });
}

function onSuccessDevice(contacts) {
    var $j = jQuery.noConflict();
    cordova.require("salesforce/util/logger").logToConsole("onSuccessDevice: received " + contacts.length + " contacts");
    $j("#div_device_contact_list").html("");
    var ul = $j('<ul data-role="listview" data-inset="true" data-theme="a" data-dividertheme="a"></ul>');
    $j("#div_device_contact_list").append(ul);
    
    ul.append($j('<li data-role="list-divider">Device Contacts: ' + contacts.length + '</li>'));
    $j.each(contacts, function(i, contact) {
           var formattedName = contact.name.formatted;
           if (formattedName) {
           var newLi = $j("<li><a href='#'>" + (i+1) + " - " + formattedName + "</a></li>");
           ul.append(newLi);
           }
           });
    
    $j("#div_device_contact_list").trigger( "create" )
}

function onErrorDevice(error) {
    cordova.require("salesforce/util/logger").logToConsole("onErrorDevice: " + JSON.stringify(error) );
    alert('Error getting device contacts!');
}

function onSuccessSfdcContacts(response) {
    var $j = jQuery.noConflict();
    cordova.require("salesforce/util/logger").logToConsole("onSuccessSfdcContacts: received " + response.totalSize + " contacts");
    
    $j("#div_sfdc_contact_list").html("");
    var ul = $j('<ul data-role="listview" data-inset="true" data-theme="a" data-dividertheme="a"></ul>');
    $j("#div_sfdc_contact_list").append(ul);
    
    ul.append($j('<li data-role="list-divider">Salesforce Contacts: ' + response.totalSize + '</li>'));
    $j.each(response.records, function(i, contact) {
		   var newLi = $j("<li><a href='#'>" + (i+1) + " - " + contact.Name + "</a></li>");
		    .click(function e(){
		    pangalan = selected_contact[i];
			forcetkClient.query("SELECT Name, Account_Name, Title, Birthdate, Phone, Email FROM Contact where Name = " + pangalan + , onSuccessSfdcContactsDetails, onErrorSfdc); 
		    }
           ul.append(newLi);
           });
    
    $j("#div_sfdc_contact_list").trigger( "create" );
}

function onSuccessSfdcContactsDetails(response) {
    var $j = jQuery.noConflict();
    cordova.require("salesforce/util/logger").logToConsole("onSuccessSfdcContacts: received " + response.totalSize + " contacts");
    
    $j.each(response.records, function(i, contact) {
           var newLi = $j("<li><a href='#'>" + (i+1) + " - " + contact.Name + "</a></li>");
           newLi.click(function(e){
				$j("#div_sfdc_contact_details").html("")
    
				e.preventDefault();
				$j.mobile.showPageLoadingMsg();
			
				var newLis = = $j('<ul data-role="listview" data-inset="true" data-theme="a" data-dividertheme="a"></ul>');
				newLis.append('<li data-role="list-divider">Name</li>');
				var Lista = $j("<li>" + contact.Name + "</a></li>"); 
				newLis.append(Lista);
				newLis.append('<li data-role="list-divider">Account Name</li>');
				var Lista1 = $j("<li>" + contact.Account_Name + "</a></li>"); 
				newLis.append(Lista1);
				newLis.append('<li data-role="list-divider">Title</li>');
				var Lista2 = $j("<li>" + contact.Title + "</a></li>"); 
				newLis.append(Lista2);
				newLis.append('<li data-role="list-divider">Birthdate</li>');
				var Lista3 = $j("<li>" + contact.Birthdate + "</a></li>"); 
				newLis.append(Lista3);
				newLis.append('<li data-role="list-divider">Phone</li>');
				var Lista4 = $j("<li>" + contact.Phone + "</a></li>"); 
				newLis.append(Lista4);
				newLis.append('<li data-role="list-divider">Email</li>');
				var Lista5 = $j("<li>" + contact.Email + "</a></li>"); 
				newLis.append(Lista5);
			
			$j("div_sfdc_contact_details").append(newLis);
			});
	});
    $j("#div_sfdc_contact_details").trigger( "create" )
}

function onSuccessSfdcAccounts(response) {
    var $j = jQuery.noConflict();
    cordova.require("salesforce/util/logger").logToConsole("onSuccessSfdcContacts: received " + response.totalSize + " contacts");
    
    $j("#div_sfdc_account_list").html("");
    var ul = $j('<ul data-role="listview" data-inset="true" data-theme="a" data-dividertheme="a"></ul>');
    $j("#div_sfdc_account_list").append(ul);
    
    ul.append($j('<li data-role="list-divider">Salesforce Contacts: ' + response.totalSize + '</li>'));
    $j.each(response.records, function(i, contact) {
		   var newLi = $j("<li><a href='#'>" + (i+1) + " - " + contact.Name + "</a></li>");
		    .click(function e(){
		    akawnt = selected_account[i];
			forcetkClient.query("SELECT Name, Account_Name, Title, Birthdate, Phone, Email FROM Account where Name = " + akawnt + , onSuccessSfdcContactsDetails, onErrorSfdc); 
		    }
           ul.append(newLi);
           });
    
    $j("#div_sfdc_account_list").trigger( "create" );
}

function onSuccessSfdcAccountsDetails(response) {
    var $j = jQuery.noConflict();
    cordova.require("salesforce/util/logger").logToConsole("onSuccessSfdcContacts: received " + response.totalSize + " contacts");
    
    $j.each(response.records, function(i, contact) {
           var newLi = $j("<li><a href='#'>" + (i+1) + " - " + contact.Name + "</a></li>");
           newLi.click(function(e){
				$j("#div_sfdc_account_details").html("")
    
				e.preventDefault();
				$j.mobile.showPageLoadingMsg();
			
				var newLis = = $j('<ul data-role="listview" data-inset="true" data-theme="a" data-dividertheme="a"></ul>');
				newLis.append('<li data-role="list-divider">Name</li>');
				var Lista = $j("<li>" + contact.Name + "</a></li>"); 
				newLis.append(Lista);
				newLis.append('<li data-role="list-divider">Account Name</li>');
				var Lista1 = $j("<li>" + contact.Account_Name + "</a></li>"); 
				newLis.append(Lista1);
				newLis.append('<li data-role="list-divider">Title</li>');
				var Lista2 = $j("<li>" + contact.Title + "</a></li>"); 
				newLis.append(Lista2);
				newLis.append('<li data-role="list-divider">Birthdate</li>');
				var Lista3 = $j("<li>" + contact.Birthdate + "</a></li>"); 
				newLis.append(Lista3);
				newLis.append('<li data-role="list-divider">Phone</li>');
				var Lista4 = $j("<li>" + contact.Phone + "</a></li>"); 
				newLis.append(Lista4);
				newLis.append('<li data-role="list-divider">Email</li>');
				var Lista5 = $j("<li>" + contact.Email + "</a></li>"); 
				newLis.append(Lista5);
			
			$j("div_sfdc_account_details").append(newLis);
			});
	});
    $j("#div_sfdc_account_details").trigger( "create" )
}

function onErrorSfdc(error) {
    cordova.require("salesforce/util/logger").logToConsole("onErrorSfdc: " + JSON.stringify(error));
    alert('Error getting sfdc contacts!');
}

function clickedCreateNewSFDCContact(){
	logToConsole("Creating new SFDC Contact.....");
	var $j = jQuery.noConflict();
	$j("#new_contact").html("");
	var cons = $j('<data-role="fieldcontain">');
	$j("#new_contact").append(cons);
	var ngalan = $j('<input type = "text" hint = "Name" id = "txtname">');
	
	
	
	forcetkClient.query("INSERT INTO Contact(Name, Account_Name, Title, Department, Birthdate, Mailing_Address, Phone, Email) VALUES ()", onSuccessSfdcContacts, onErrorSfdc); 		
	
}

function createNewSFDCContact(response){
	alert("Success in creating new SFDC contact!");
}





